package com.company;

class QuizImpl implements Quiz {

    private int digit;

    public QuizImpl() {
        this.digit = 254;    // zmienna moze ulegac zmianie!
    }
    // implementacja metody isCorrectValue...
    public void isCorrectValue(int digit) throws Quiz.ParamTooLarge, Quiz.ParamTooSmall {
        if(digit>this.digit) {throw new ParamTooLarge();}
        else if(digit<this.digit) {throw new ParamTooSmall();}
    }
}
