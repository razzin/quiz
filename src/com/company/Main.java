package com.company;

import java.util.concurrent.ThreadLocalRandom;

public class Main {

    public static void main(String[] args) {
        // write your code here
        QuizImpl quiz = new QuizImpl();

        int min=Quiz.MIN_VALUE,max=Quiz.MAX_VALUE;
        int digit;

        for(int counter = 1; ;counter++) {
            digit = (min + max) / 2;
            try {

                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            } catch(Quiz.ParamTooLarge e) {
                max=digit-1;
                System.out.println("Argument za duzy!!!");
            } catch(Quiz.ParamTooSmall e) {
                min=digit+1;
                System.out.println("Argument za maly!!!");
            }
        }
    }
}
